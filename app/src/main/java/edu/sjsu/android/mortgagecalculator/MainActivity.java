/* Author: James Nguyen
 */
package edu.sjsu.android.mortgagecalculator;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private EditText text;
    private RadioGroup rdioGrp;
    private SeekBar sb;
    private CheckBox checkBox;
    private TextView result;
    private TextView interestRateView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        text = (EditText) findViewById(R.id.editText);
        rdioGrp = (RadioGroup) findViewById(R.id.radioGroup);
        checkBox = (CheckBox) findViewById(R.id.checkBox);
        sb = findViewById(R.id.seekBar);
        interestRateView = (TextView) findViewById(R.id.textView2);
        result = (TextView) findViewById(R.id.textView);

        sb.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                interestRateView.setText("Interest Rate: " + i + "%");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.button:

                // This gives us the loan term in months (N)
                int radioID = rdioGrp.getCheckedRadioButtonId();
                RadioButton selectedBtn = (RadioButton) findViewById(radioID);
                float N = Float.parseFloat(selectedBtn.getText().toString()) * 12;

                // This gives us interest rate (J)
                float J = (float) sb.getProgress() / 1200;

                // this gives us our amount borrowed (P)
                if (text.getText().length() == 0) {
                    Toast.makeText(this, "Please enter a valid number",
                            Toast.LENGTH_LONG).show();
                    return;
                }
                float P = Float.parseFloat(text.getText().toString());

                float M;
                if (J == 0) {
                    M = CalculationUtil.monthlyPayment(P, N, checkBox.isChecked());
                } else {
                    M = CalculationUtil.monthlyPayment(P, J, N, checkBox.isChecked());
                }
                result.setText("Monthly Payment: $" + String.format("%.2f", M));
                break;
        }
    }
}