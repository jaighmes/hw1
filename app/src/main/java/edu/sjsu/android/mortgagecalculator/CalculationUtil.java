/* Author: James Nguyen
 */
package edu.sjsu.android.mortgagecalculator;

public class CalculationUtil {

    public static float monthlyPayment(float P, float J, float N, boolean T) {
        float M = (float) ( P * ( J / ( 1 - Math.pow( 1 + J, -N ) ) ) );
        if (T) M += .001 * P; // add tax if true
        return M;
    }

    public static float monthlyPayment(float P, float N, boolean T) {
        float M = P / N;
        if (T) M += .001 * P; // add tax if true
        return M;
    }
}
